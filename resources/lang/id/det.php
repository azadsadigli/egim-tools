<?php

return [
  'About_us' => 'Egim.io adalah alat gratis online bagi pengembang untuk merancang halaman web yang lebih baik.',
  'About_finance' => 'Fungsionalitas kedua dari Egim.io adalah untuk membantu orang-orang di bidang keuangan untuk mempercepat perhitungan mereka dengan sistem waktu nyata.',
];
