<?php

return [
  'About_us' => 'Egim.io es una herramienta gratuita en línea para que los desarrolladores diseñen mejores páginas web.',
  'About_finance' => 'La segunda funcionalidad de Egim.io es ayudar a las personas en el área de finanzas a acelerar su cálculo mediante el sistema en tiempo real.',
];
