<?php

return [
  'About_us' => 'Egim.io ist ein kostenloses Online-Tool für Entwickler, um bessere Webseiten zu erstellen.',
  'About_finance' => 'Die zweite Funktion von Egim.io besteht darin, den Menschen im Finanzbereich dabei zu helfen, ihre Berechnung durch ein Echtzeitsystem zu beschleunigen.',
];
