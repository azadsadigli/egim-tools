<?php

return [
  'About_us' => 'Egim.io是一个在线免费工具，供开发人员设计更好的网页.',
  'About_finance' => 'Egim.io的第二个功能是帮助金融领域的人们通过实时系统加速计算.',
];
